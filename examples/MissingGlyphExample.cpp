//
// SimpleLASiExample.cpp
//

#include <iostream>
#include <fstream>
#include <stdexcept>
#include <LASi.h>

using namespace LASi;
using namespace std;

int main(const int argc, char* const argv[]) 
{
  ofstream strm;

  try {
    PostscriptDocument doc;

    //
    // Set font to generic "serif":
    //
    doc.osBody() << setFont("Arial") << setFontSize(18) << endl;
    //char testString[]={0xe2,0xab,0xb4,0x00};
    const char *testString="Unicode U+2AF4 — U+2AF8 glyphs are : ⫴⫵⫶⫷⫸.";
    //
    // Show the string:
    //
    doc.osBody() << "28 560 moveto" << endl;
    doc.osBody() << show(testString);

    //
    // Postscript showpage:
    //
    doc.osBody() << "showpage" << endl;
    
    if (argc == 1) {
      doc.write(cout);
    }
    else {
      strm.open(argv[1]);
      doc.write(strm);
      strm.close();
    }
    
  } catch (runtime_error& e) {
  
    cerr << e.what() << endl;
    return 1;
    
  }

  return 0;
  
}

