//
// SimpleLASiExample.cpp
//

#include <iostream>
#include <fstream>
#include <stdexcept>
#include <LASi.h>

using namespace LASi;
using namespace std;

int main(const int argc, char* const argv[]) 
{
  ofstream strm;

  try {
    PostscriptDocument doc;

    double xAdvance,yMinimum,yMaximum,lineSpacing,yDelta;
    double llx,urx,lly,ury;
    //
    // Set font to generic "serif":
    //
    doc.osBody() << setFont("serif") << setFontSize(18) << endl;
    //
    // "Hello" in formal Arabic, English, Hebrew, and Hindi.
    //
    // See http://www.unifont.org/fontguide/
    // if you need fonts for these scripts:
    //
    char testString[]="السلام عليكم Hello שלום नमस्ते";
    //
    // Get string dimensions:
    //
    doc.get_dimensions(testString,&lineSpacing,&xAdvance,&yMinimum,&yMaximum);
    //
    // cerr << endl;
    // cerr << "X-ADVANCE  : " << xAdvance << endl;
    // cerr << "Y-MINIMUM  : " << yMinimum << endl;
    // cerr << "Y-MAXIMUM  : " << yMaximum << endl;
    // cerr << "LINESPACING: " << lineSpacing << endl;
    //
    
    //
    // Draw a rectangle showing the bounding box of the string:
    //
    yDelta=yMaximum-yMinimum;
    doc.osBody() << "gsave newpath" << endl;
    doc.osBody() << "1.00 0.00 0.00 setrgbcolor" << endl;
    doc.osBody() << "100 600 moveto" << endl;
    doc.osBody() << 0 << " " << yMinimum << " rmoveto " << endl;
    doc.osBody() << 0 << " " << yDelta << " rlineto " << endl;
    doc.osBody() << xAdvance << " " << 0 << " rlineto " << endl;
    doc.osBody() << 0 << " " << -yDelta << " rlineto " << endl;
    doc.osBody() << "closepath" << endl;
    doc.osBody() << "stroke grestore" << endl;

    //
    // Show the string:
    //
    doc.osBody() << "100 600 moveto" << endl;
    doc.osBody() << show(testString);

    //
    // Advance to the next line:
    //
    doc.osBody() << "100 600 moveto" << endl;
    doc.osBody() << "0 " << -lineSpacing << " rmoveto" << endl;
    //
    // Print the test string again:
    //
    doc.osBody() << show(testString);

    //
    // Postscript showpage:
    //
    doc.osBody() << "showpage" << endl;

    // Calculate total bounding box
    llx = 100-1;
    urx = 100+xAdvance+1;
    lly = 600-lineSpacing+yMinimum-1;
    ury = 600+yMaximum+1;
    
    // Write out postscript document, including bounding box. 
    // If the bounding box arguments are omitted then no
    // bounding box is included in the file.
    //
    if (argc == 1) {
      doc.write(cout,llx,lly,urx,ury);
    }
    else {
      strm.open(argv[1]);
      doc.write(strm,llx,lly,urx,ury);
      strm.close();
    }
    
  } catch (runtime_error& e) {
  
    cerr << e.what() << endl;
    return 1;
    
  }

  return 0;
  
}

