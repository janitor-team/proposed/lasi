# cmake/modules/lasi.cmake
# Module for determining all configuration variables for libLASi.

# libraries are all shared by default
option(BUILD_SHARED_LIBS "Build shared libraries" ON)

# for win32 we set the DLLFLAG (used in lasi.pc)
# to import the functions correctly from the dll
if(WIN32 AND BUILD_SHARED_LIBS)
  set( DLLFLAG "-DLASi_DLL" )
endif(WIN32 AND BUILD_SHARED_LIBS)

# Set executable suffix for the Makefiles for the installation tree examples.
# WIN32 covers CYGWIN as well (and possibly MINGW, but we will make sure).
if(WIN32 OR MINGW)
  set(EXEEXT .exe)
endif(WIN32 OR MINGW)

# Useful macro for helping to do version tests.  May be needed for future
# development of CMake build system for libLASi.

macro(TRANSFORM_VERSION _numerical_result _version)
# _version must be a period-delimited triplet string of the form
# "major.minor.patch".
# This macro transforms that into a numerical result that can be compared.
if(${_version} MATCHES "^[0-9]*\\.[0-9]*\\.[0-9]*$")
  string(REGEX REPLACE "^([0-9]*)\\.[0-9]*\\.[0-9]*$" "\\1" _major ${_version})
  string(REGEX REPLACE "^[0-9]*\\.([0-9]*)\\.[0-9]*$" "\\1" _minor ${_version})
  string(REGEX REPLACE "^[0-9]*\\.[0-9]*\\.([0-9]*)$" "\\1" _patch ${_version})
  math(EXPR ${_numerical_result}
  "${_major}*1000000 + ${_minor}*1000 + ${_patch}
  ")
else(${_version} MATCHES "^[0-9]*\\.[0-9]*\\.[0-9]*$")
  set(${_numerical_result} 0)
endif(${_version} MATCHES "^[0-9]*\\.[0-9]*\\.[0-9]*$")
endmacro(TRANSFORM_VERSION)

# On windows systems the math library is not separated so do not specify
# it unless you are on a non-windows system.
if(NOT WIN32)
  set(MATH_LIB m)
endif(NOT WIN32)

# Fundamental install locations (used in, e.g., configured lasi.pc file).
include(instdirs)

# =======================================================================
# pkg-config support
# =======================================================================

include(pkg-config)

pkg_check_modules(PANGO pango)
if(PANGO_FOUND)
  include_directories(${PANGO_INCLUDE_DIRS})
  link_directories(${PANGO_LIBRARY_DIRS})
  link_libraries(${PANGO_LIBRARIES})
  add_definitions(${PANGO_CFLAGS})
  set(PANGO_RPATH ${PANGO_LIBDIR})
else(PANGO_FOUND)
  message(FATAL_ERROR "Pango "
  "(http://ftp.gnome.org/pub/GNOME/sources/pango/) is required to build "
  "libLASi.")
endif(PANGO_FOUND)

pkg_check_modules(PANGOFT2 pangoft2)
if(PANGOFT2_FOUND)
  include_directories(${PANGOFT2_INCLUDE_DIRS})
  link_directories(${PANGOFT2_LIBRARY_DIRS})
  link_libraries(${PANGOFT2_LIBRARIES})
  add_definitions(${PANGOFT2_CFLAGS})
else(PANGOFT2_FOUND)
  message(FATAL_ERROR "Pangoft2 not found")
endif(PANGOFT2_FOUND)

pkg_check_modules(FT2 freetype2)
if(FT2_FOUND)
  include_directories(${FT2_INCLUDE_DIRS})
  link_directories(${FT2_LIBRARY_DIRS})
  link_libraries(${FT2_LIBRARIES})
  add_definitions(${FT2_CFLAGS})
else(FT2_FOUND)
  message(FATAL_ERROR "ft2 not found")
endif(FT2_FOUND)

if(CMAKE_SYSTEM_NAME STREQUAL "Darwin")
  # No rpath on Darwin. Setting it will only cause trouble.
else(CMAKE_SYSTEM_NAME STREQUAL "Darwin")
  option(USE_RPATH "Use -rpath when linking libraries, executables" ON)
endif(CMAKE_SYSTEM_NAME STREQUAL "Darwin")

# =======================================================================
# Test of FreeType API.
# =======================================================================
if(FT2_VERSION)
  transform_version(NUMERICAL_FT2_MINIMUM_VERSION "9.0.0")
  transform_version(NUMERICAL_FT2_2_2_VERSION "9.10.0")
  transform_version(NUMERICAL_FT2_VERSION "${FT2_VERSION}")
  if(NUMERICAL_FT2_VERSION LESS "${NUMERICAL_FT2_MINIMUM_VERSION}")
    message(FATAL 
    "FreeType2 Library version 9.0.0+ is required.  "
    "Library version is ${FT2_VERSION}"
    )
  elseif(NUMERICAL_FT2_VERSION LESS "${NUMERICAL_FT2_2_2_VERSION}")
    set(HAVE_FT2_2PLUS OFF)
    message(STATUS
    "FOUND FreeType 2.1 (OLD API): Library version is ${FT2_VERSION}"
    )
  else(NUMERICAL_FT2_VERSION LESS "${NUMERICAL_FT2_MINIMUM_VERSION}")
    set(HAVE_FT2_2PLUS ON)
    message(STATUS
    "FOUND FreeType 2.2+ (NEW API): Library version is ${FT2_VERSION}"
    )
  endif(NUMERICAL_FT2_VERSION LESS "${NUMERICAL_FT2_MINIMUM_VERSION}")
else(FT2_VERSION) 
  message(FATAL "Cannot determine FreeType2 Library version with pkg-config")
endif(FT2_VERSION) 

# =======================================================================
# doxygen support
# =======================================================================

find_package(Doxygen)
if(NOT DOXYGEN_EXECUTABLE)
  message(STATUS "WARNING: Doxygen not found so documentation not generated.")
endif(NOT DOXYGEN_EXECUTABLE)

# =======================================================================
# Prepare summary macro that is used later.
# =======================================================================

include(summary)
