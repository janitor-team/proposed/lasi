# cmake/modules/installdirs.cmake

# Module for determining all installation directories for libLASi.

# Note, use absolute install locations only since relative ones are
# automatically prefixed with ${CMAKE_INSTALL_PREFIX} which may destroy
# the fine-tuning of installation locations that we desire.

# Cached install locations following what is done for the autotools configure
# script: (CMAKE_INSTALL_SBINDIR, CMAKE_INSTALL_LIBEXECDIR,
# CMAKE_INSTALL_SHAREDSTATEDIR, CMAKE_INSTALL_LOCALSTATEDIR, and
# CMAKE_INSTALL_OLDINCLUDEDIR not set because PLplot does not use those
# install locations).

set(
CMAKE_INSTALL_EXEC_PREFIX 
${CMAKE_INSTALL_PREFIX}
CACHE PATH "install location for architecture-dependent files"
)

set(
CMAKE_INSTALL_BINDIR
${CMAKE_INSTALL_EXEC_PREFIX}/bin
CACHE PATH "install location for user executables"
)

set(
CMAKE_INSTALL_DATADIR
${CMAKE_INSTALL_PREFIX}/share
CACHE PATH "install location for read-only architecture-independent data"
)

set(
CMAKE_INSTALL_LIBDIR
${CMAKE_INSTALL_EXEC_PREFIX}/lib
CACHE PATH "install location for object code libraries"
)

set(
CMAKE_INSTALL_INCLUDEDIR
${CMAKE_INSTALL_PREFIX}/include
CACHE PATH "install location for C header files"
)

set(
CMAKE_INSTALL_INFODIR
${CMAKE_INSTALL_DATADIR}/info
CACHE PATH "install location for info documentation"
)

set(
CMAKE_INSTALL_MANDIR
${CMAKE_INSTALL_DATADIR}/man
CACHE PATH "install location for man documentation"
)

set(prefix ${CMAKE_INSTALL_PREFIX})
set(exec_prefix ${CMAKE_INSTALL_EXEC_PREFIX})
set(libdir ${CMAKE_INSTALL_LIBDIR})
set(bindir ${CMAKE_INSTALL_BINDIR})
set(includedir ${CMAKE_INSTALL_INCLUDEDIR})
set(docdir ${CMAKE_INSTALL_DATADIR}/doc/libLASi-${VERSION})

set(DATA_DIR ${CMAKE_INSTALL_DATADIR}/${PACKAGE}${VERSION})
