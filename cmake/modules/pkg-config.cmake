# cmake/modules/pkg-config.cmake

# Make PKG_CONFIG_EXECUTABLE available as well as the pkg_check_modules macro.
find_package(PkgConfig)

if(PKG_CONFIG_EXECUTABLE)
  message(STATUS "Looking for pkg-config - found")
  set(pkg_config_true "")
  set(PKG_CONFIG_DIR ${libdir}/pkgconfig)
else(PKG_CONFIG_EXECUTABLE)
  message(STATUS "Looking for pkg-config - not found")
  set(pkg_config_true "#")
  message(FATAL_ERROR "Build of libLASi requires pkg-config.")
endif(PKG_CONFIG_EXECUTABLE)
