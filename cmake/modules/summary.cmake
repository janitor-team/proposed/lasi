# cmake/modules/summary.cmake
# Macro for outputting all the most important CMake variables for libLASi

macro(summary)
set(_output_results "
Summary of CMake build system results for libLASi

Install location variables which can be set by the user:
CMAKE_INSTALL_PREFIX:		${CMAKE_INSTALL_PREFIX}
CMAKE_INSTALL_EXEC_PREFIX:	${CMAKE_INSTALL_EXEC_PREFIX}
CMAKE_INSTALL_BINDIR:		${CMAKE_INSTALL_BINDIR}
CMAKE_INSTALL_DATADIR:		${CMAKE_INSTALL_DATADIR}
CMAKE_INSTALL_LIBDIR:		${CMAKE_INSTALL_LIBDIR}
CMAKE_INSTALL_INCLUDEDIR:	${CMAKE_INSTALL_INCLUDEDIR}
CMAKE_INSTALL_INFODIR:		${CMAKE_INSTALL_INFODIR}
CMAKE_INSTALL_MANDIR:		${CMAKE_INSTALL_MANDIR}

Other important CMake variables:

CMAKE_SYSTEM_NAME:		${CMAKE_SYSTEM_NAME}
UNIX:				${UNIX}
WIN32:				${WIN32}
APPLE:				${APPLE}
MSVC:				${MSVC}	(MSVC_VERSION:	${MSVC_VERSION})
MINGW:				${MINGW}
MSYS:				${MSYS}
CYGWIN:				${CYGWIN}
BORLAND:			${BORLAND}
WATCOM:				${WATCOM}

CMAKE_BUILD_TYPE:		${CMAKE_BUILD_TYPE}
CMAKE_CXX_COMPILER:		${CMAKE_CXX_COMPILER}")
if(NOT CMAKE_BUILD_TYPE)
set(_output_results
"${_output_results}
CMAKE_CXX_FLAGS:		${CMAKE_CXX_FLAGS}")
elseif(CMAKE_BUILD_TYPE STREQUAL "Release")
set(_output_results
"${_output_results}
CMAKE_CXX_FLAGS_RELEASE:	${CMAKE_CXX_FLAGS_RELEASE}")
elseif(CMAKE_BUILD_TYPE STREQUAL "Debug")
set(_output_results
"${_output_results}
CMAKE_CXX_FLAGS_DEBUG:		${CMAKE_CXX_FLAGS_DEBUG}")
elseif(CMAKE_BUILD_TYPE STREQUAL "RelWithDebInfo")
set(_output_results
"${_output_results}
CMAKE_CXX_FLAGS_RELWITHDEBINFO:	${CMAKE_CXX_FLAGS_RELWITHDEBINFO}")
elseif(CMAKE_BUILD_TYPE STREQUAL "MinSizeRel")
set(_output_results
"${_output_results}
CMAKE_CXX_FLAGS_MINSIZEREL:	${CMAKE_CXX_FLAGS_MINSIZEREL}")
else(NOT CMAKE_BUILD_TYPE)
  message(FATAL_ERROR "Invalid CMAKE_BUILD_TYPE = ${CMAKE_BUILD_TYPE}")
endif(NOT CMAKE_BUILD_TYPE)

message("${_output_results}")
endmacro(summary)
