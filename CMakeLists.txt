# Top-level CMakeLists.txt for libLASi
###
### Process this file with cmake to produce Makefile
###

# Version 2.4.5 or above of cmake is required!
CMAKE_MINIMUM_REQUIRED(VERSION 2.4.5 FATAL_ERROR)

project(libLASi CXX)

set(PACKAGE lasi)

# Location where libLASi cmake build system first looks for cmake modules.
set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake/modules)

# Version information.

include(lasi_version)

enable_testing()

# parameters to control overall cmake behaviour.

# Configure libLASi component variables....

include(lasi)

# Use configured variables to process configurable top-level files.

configure_file(
${CMAKE_CURRENT_SOURCE_DIR}/config.h.cmake 
${CMAKE_CURRENT_BINARY_DIR}/config.h
)

configure_file(
${CMAKE_CURRENT_SOURCE_DIR}/lasi.pc.in
${CMAKE_CURRENT_BINARY_DIR}/lasi.pc
)

install(FILES
${CMAKE_CURRENT_BINARY_DIR}/lasi.pc 
DESTINATION ${PKG_CONFIG_DIR}
)

install(FILES include/LASi.h DESTINATION ${includedir})

add_subdirectory(src)

add_subdirectory(examples)

#
# Generating documentation with doxygen
#
if(DOXYGEN_EXECUTABLE AND UNIX)
  
  # N.B. Both the following custom rules assume the doc directory exists
  # at make time, and the following install(DIRECTORY... must have doc exist
  # at cmake time.  Therefore, create the doc directory at CMake time.
  # (Linux experimentation indicates this is a no-op if the empty or
  # non-empty directory already exists.)
  file(MAKE_DIRECTORY ${CMAKE_SOURCE_DIR}/doc)

  # The initial rm command gets rid of everything previously built by this
  # custom command.
  add_custom_command(
  OUTPUT ${CMAKE_SOURCE_DIR}/doc/user/html/index.html
  COMMAND rm -rf ${CMAKE_SOURCE_DIR}/doc/user
  COMMAND mkdir ${CMAKE_SOURCE_DIR}/doc/user
  COMMAND ${DOXYGEN_EXECUTABLE} ${CMAKE_SOURCE_DIR}/Doxyfile.user
  DEPENDS ${CMAKE_SOURCE_DIR}/Doxyfile.user
  WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
  )

  # The initial rm command gets rid of everything previously built by this
  # custom command.
  add_custom_command(
  OUTPUT ${CMAKE_SOURCE_DIR}/doc/developer/html/index.html
  COMMAND rm -rf ${CMAKE_SOURCE_DIR}/doc/developer
  COMMAND mkdir ${CMAKE_SOURCE_DIR}/doc/developer
  COMMAND ${DOXYGEN_EXECUTABLE} ${CMAKE_SOURCE_DIR}/Doxyfile.developer
  DEPENDS ${CMAKE_SOURCE_DIR}/Doxyfile.developer
  WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
  )

  add_custom_target(
  documentation ALL
  DEPENDS
  ${CMAKE_SOURCE_DIR}/doc/user/html/index.html
  ${CMAKE_SOURCE_DIR}/doc/developer/html/index.html
  )
  
  # Install the documentation generated at "make" time.
  install(DIRECTORY ${CMAKE_SOURCE_DIR}/doc/ DESTINATION ${docdir}/html)

endif(DOXYGEN_EXECUTABLE AND UNIX)

#
# Packing stuff
#
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY
"The PostScript Complex Text Layout Library, LASi")
set(CPACK_PACKAGE_VENDOR "Ed Trager <ed.trager@gmail.com>")
set(CPACK_PACKAGE_DESCRIPTION_FILE ${CMAKE_CURRENT_SOURCE_DIR}/README)
if(WIN32)
  set(CPACK_GENERATOR ZIP)
else(WIN32)
  set(CPACK_GENERATOR TGZ)
endif(WIN32)
set(
CPACK_SOURCE_PACKAGE_FILE_NAME
"libLASi-${VERSION}"
CACHE INTERNAL "tarball basename"
)
if(WIN32)
  set(CPACK_SOURCE_GENERATOR ZIP)
else(WIN32)
  set(CPACK_SOURCE_GENERATOR TGZ)
endif(WIN32)
# The following components are regex's to match anywhere (unless anchored)
# in absolute path + filename to find files or directories to be excluded
# from source tarball.
set(CPACK_SOURCE_IGNORE_FILES
"~$"
"^${PROJECT_SOURCE_DIR}.*\\\\.svn/"
)
#message("CPACK_SOURCE_IGNORE_FILES = ${CPACK_SOURCE_IGNORE_FILES}")
include(CPack)
summary()
