/** @file
 *
 * Added to LASi on 2004.12.14 by ET.
 * 
 * StringDimensions is a simple class for maintaining the relevant
 * dimensions of a text string realized (i.e., drawn) in a given
 * font face and font size.
 *
 * libLASi provides a C++ output stream interface for writing 
 * multi-language Postscript documents.
 * Copyright (C) 2003, 2004 Larry Siden.
 *
 * See README file in project root directory for copyright and contact info.
 * See COPYING file in project root for terms of re-distribution.
 */


/**
  * Class StringDimensions maintains the relevant
  * dimensions of a text string realized (i.e., drawn) in a given
  * font face and font size.  Used internally by LASi.
  */
class StringDimensions {

	private:
	
	double xadv;
	double ymin;
	double ymax;
	double lineSpacingFactor;
	
	public:
	
	// Constructor:
	StringDimensions(double xAdv=0.0, double yMin=0.0,double yMax=0.0,double spacingFactor=1.2){
		xadv = xAdv;
		ymin = yMin;
		ymax = yMax;
		lineSpacingFactor = spacingFactor;
		
	}
	
	//
	// Set methods: Accrue mins and maxs:
	// On the Y-axis, just take the true min and max.
	// 
	// On the X-axis, sum up all the individual x-advances
	// in order to get the overall bounding box:
	// 
	
	/**
	  * Sum up the individual x advances of each glyph:
	  */
	void accrueXAdvance(const double xAdv){ xadv += xAdv; }
	
	/**
	  * Keep a running tab on the minimum y value (descent) from all glyphs:
	  */
	void setYMin(const double yMin){ if( yMin < ymin ) ymin = yMin; }
	/**
	  * Keep a running tab on the maximum y value (ascent) from all glyphs:
	  */
	void setYMax(const double yMax){ if( yMax > ymax ) ymax = yMax; }
	
	// Get methods:
	const double getXAdvance(){ return xadv; }
	const double getYMin(){ return ymin; }
	const double getYMax(){ return ymax; }
	
	/**
	  * Line spacing is just the maximum ascent minus the maximum descent
	  * multiplied by the line spacing factor:
	  */
	const double getLineSpacing(){ return (ymax-ymin)*lineSpacingFactor; }
	
};

