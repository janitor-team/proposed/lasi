/** @file
 * libLASi provides a C++ output stream interface for writing multi-language Postscript documents.
 * Copyright (C) 2003  Larry Siden.
 * See README file in project root directory for copyright and contact info.
 * See COPYING file in project root for terms of re-distribution.
 */

#ifndef DRAW_GLYPH_H
#define DRAW_GLYPH_H

#include <iostream>
#include <pango/pango.h>
#include <freetype/freetype.h>

/** Generate the Postscript commands to draw the glyph
  * using the font in pPangoCtx.
  */
void writeGlyphRoutine(std::ostream& os, PangoContext * const pPangoCtx, FT_Glyph glyph);
#endif
