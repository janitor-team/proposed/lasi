/** @file
 * libLASi provides a C++ output stream interface for writing 
 * multi-language Postscript documents.
 * Copyright (C) 2003, 2004  Larry Siden.
 * See README file in project root directory for copyright and contact info.
 * See COPYING file in project root for terms of re-distribution.
 */

#include <stdexcept>

#include <ft2build.h>
#include FT_FREETYPE_H

#include <freetype/ftglyph.h>

#include <cassert>
#include "util.h"
#include "glyphMgr.h"

using namespace std;

/** Manage FT_Glyph by insuring that resources are freed when done.
 */
FreetypeGlyphMgr::FreetypeGlyphMgr() : _glyph((FT_Glyph)0) {
  _glyph = (FT_Glyph)0;
}

FreetypeGlyphMgr::FreetypeGlyphMgr(FT_Glyph glyph) : _glyph(_glyph) {}

FreetypeGlyphMgr::FreetypeGlyphMgr(const FreetypeGlyphMgr& ftgm) {
  //evalReturnCode(FT_Glyph_Copy(ftgm._glyph, &_glyph), "FT_Glyph_Copy() in FreetypeGlyphMgr(const FreetypeGlyphMgr& ftgm)");
  if (ftgm._glyph) {
    int return_code = FT_Glyph_Copy(ftgm._glyph, &_glyph);
    assert(0 == return_code);
  }
  else
    _glyph = 0;
}

FreetypeGlyphMgr& FreetypeGlyphMgr::operator=(const FreetypeGlyphMgr& ftgm) {
  if (this != &ftgm) {
    if (_glyph) {
      FT_Done_Glyph(_glyph);
    }
    //evalReturnCode(FT_Glyph_Copy(ftgm._glyph, &_glyph), "FT_Glyph_Copy() in operator=(const FreetypeGlyphMgr& ftgm)");
    if (ftgm._glyph) {
      int return_code = FT_Glyph_Copy(ftgm._glyph, &_glyph);
      assert(0 == return_code);
    }
    else
      _glyph = 0;
  }
  return *this;
}

FreetypeGlyphMgr::~FreetypeGlyphMgr() {
  if (_glyph) {
    FT_Done_Glyph(_glyph);
  }
}

FreetypeGlyphMgr::operator FT_Glyph() const {
  return _glyph;
}

void FreetypeGlyphMgr::assign(const FT_Glyph glyph) {
  _glyph = glyph;
}
