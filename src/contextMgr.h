/** @file
 * libLASi provides a C++ output stream interface for writing multi-language Postscript documents.
 * Copyright (C) 2003  Larry Siden.
 * See README file in project root directory for copyright and contact info.
 * See COPYING file in project root for terms of re-distribution.
 */

#ifndef CONTEXT_MGR_H
#define CONTEXT_MGR_H

#include <pango/pango.h>
#include <pango/pangoft2.h>
#include "manager.h"

/** Manage PangoContext*
 */
class ContextMgr : public Manager<PangoContext*> {
  public:
    ContextMgr(
        const char* const lang="en_US.UTF-8",
        const int dpiX = 72,
        const int dpiY = 72) {
      _t = pango_ft2_get_context(dpiX, dpiY);
      pango_context_set_language(_t, pango_language_from_string(lang));
      pango_context_set_base_dir(_t, PANGO_DIRECTION_LTR);
    }

    ~ContextMgr() {
      if (_t && isOwner())
      g_object_unref(_t);
    }
};
#endif
