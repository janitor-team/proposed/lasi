/** @file
 * Convert return-code of Freetype library calls to std::runtime_error.
 * Usage: place every call to Freetype library inside FtEval::eval().
 *
 * libLASi provides a C++ output stream interface for writing 
 * multi-language Postscript documents.
 * Copyright (C) 2003, 2004 Larry Siden.
 *
 * See README file in project root directory for copyright and contact info.
 * See COPYING file in project root for terms of re-distribution.
 */

#ifndef MANAGER_H
#define MANAGER_H

#include <stdexcept>
#include <string>
#include <iostream>

/** Manager template.
 * Make underlying types exception-safe.
 */
template <class T>
class Manager {
  protected: 
    T _t;

  public: 
    Manager() : _t(0), _isOwner(false) {}
    Manager<T>(const T t) : _t(t), _isOwner(true) {}
    operator T() const {return _t;}

  protected:
    bool isOwner() const {return _isOwner;}
    void release() {_isOwner = false;}

  private:
    bool _isOwner;
};

#endif
